export RDTSC_FREQUENCY=3500
salt-key -Ay
salt 'node-*' test.ping
sleep 3
salt 'node-*' test.ping
sleep 3
salt '*' cmd.run "echo $(cat ~vagrant/.ssh/id_rsa.pub) >> .ssh/authorized_keys" runas=vagrant
salt '*' cmd.run "echo $(cat ~vagrant/.ssh/id_rsa.pub) >> .ssh/authorized_keys" runas=root
salt '*' cmd.run "echo $(sudo cat ~root/.ssh/id_rsa.pub) >> .ssh/authorized_keys" runas=vagrant
salt '*' cmd.run "echo $(sudo cat ~root/.ssh/id_rsa.pub) >> .ssh/authorized_keys" runas=root
salt '*' cmd.run "zsh -c 'source .zshrc'" runas=vagrant
salt '*' cmd.run "zsh -c 'source .zshrc'" runas=root