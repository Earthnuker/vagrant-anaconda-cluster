To boot the cluster change then `num_nodes` and `num_cpus` variable in the Vagrantfile, then run
```bash
vagrant up
vagrant ssh -c 'sudo -E sh /vagrant/setup_ssh_keys.sh' # to distribute SSH-Keys among nodes
vagrant ssh
```
this will do the following:
- boot the VMs
- Install and configure Salt
- Setup Networking between the VMs
- Install avahi/mdns so they can be reached by hostname
- Install ZSH and Antigen
- Change default shell to ZSH
- Generate SSH-Keys for the `root` and `vagrant` user
- Generate empty `authorized_keys` files for `root` and `vagrant` user
- Install Miniconda, then Download and install Anaconda and Paramiko
- Copy the SSH-Keys from the Master-Node to the other nodes in the Cluster using Salt

to start a Dask Cluster run the following commands:
```bash
sudo -E salt 'node-*' cmd.run 'export LC_ALL=C.UTF-8;source activate;dask-worker --nprocs $(nproc) master.local:8786' runas=vagrant shell=/usr/bin/zsh bg=True
dask-scheduler
```

then open a new terminal, cd into the folder with the cloned Repo and run `vagrant ssh` to open a second SSH-Connection so you can run ipython, etc
