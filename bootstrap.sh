until sudo apt-get update -y; do sleep 1; done
until sudo apt-get install -y git avahi-daemon avahi-discover avahi-utils libnss-mdns mdns-scan zsh; do  sleep 1; done

mkdir .antigen
sudo mkdir /root/.antigen
until curl -qL https://git.io/antigen > ./.antigen/antigen.zsh; do  sleep 1; done;

cat ./.antigen/antigen.zsh|sudo tee /root/.antigen/antigen.zsh > /dev/null
(tee .zshrc | sudo tee /root/.zshrc > /dev/null) <<EOF
source ~/.antigen/antigen.zsh
antigen use oh-my-zsh
antigen bundle command-not-found
antigen bundle zsh-users/zsh-syntax-highlighting
antigen theme gentoo
antigen apply
export RDTSC_FREQUENCY=3500
EOF
sudo chsh -s $(which zsh) root
sudo chsh -s $(which zsh) vagrant

(sudo tee /etc/zsh/zprofile|sudo tee /etc/profile > /dev/null) <<EOF
export PATH=\$PATH:~vagrant/miniconda3/bin
export RDTSC_FREQUENCY=3500
EOF

yes "" | ssh-keygen -N ""
yes "" | sudo ssh-keygen -N ""
touch ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
sudo touch ~root/.ssh/authorized_keys
sudo chmod 600 ~root/.ssh/authorized_keys

until curl -qL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh > miniconda3.sh; do  sleep 1; done;
bash ./miniconda3.sh -b
export PATH=$PATH:~vagrant/miniconda3/bin
conda install anaconda paramiko -y -q
